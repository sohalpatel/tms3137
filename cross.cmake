#################################################################
# @file   TI_cgt_arm_5.2.6.cmake
# @brief  toolchain file for CMake to use TI cross-compiler Version 5.2.6
# @author Thomas Tuul, Sohal Patel
# Copyright (C) 2016-17 Franka Emika GmbH
#################################################################

# see
# https://e2e.ti.com/support/development_tools/compiler/f/343/t/310610
# and
# http://www.vtk.org/Wiki/CMake_Cross_Compiling

# usage:
# cmake -DCMAKE_TOOLCHAIN_FILE="TI_cgt_arm_5.2.6.cmake"

#################################################################
# Set Toolchain Programs and Paths
#################################################################

# Targeting an embedded system, no OS.
set( CMAKE_SYSTEM_NAME Generic )
set( CMAKE_SYSTEM_PROCESSOR TMS570LS2124 )

set( TOOL_ROOT "/mnt/c/ti/ccsv8/tools/compiler/ti-cgt-arm_18.12.0.LTS" )

# specify the cross compiler
set( CMAKE_C_COMPILER   "${TOOL_ROOT}/bin/armcl.exe" )
set( CMAKE_CXX_COMPILER "${CMAKE_C_COMPILER}" )
set( CMAKE_ASM_COMPILER "${CMAKE_C_COMPILER}" )

# specify the make tool
#set( CMAKE_MAKE_PROGRAM "C:/ti/ccsv6/utils/bin/gmake" )


# skip compiler tests so there is no need to force
# the compiler via CMAKE_FORCE_C_COMPILER
# but CMAKE_C_FLAGS has to specified due to tests
set( CMAKE_ASM_COMPILER_WORKS         0 )
set( CMAKE_C_COMPILER_WORKS           0 )
set( CMAKE_CXX_COMPILER_WORKS         0 )
set( CMAKE_DETERMINE_ASM_ABI_COMPILED 0 )
set( CMAKE_DETERMINE_C_ABI_COMPILED   0 )
set( CMAKE_DETERMINE_CXX_ABI_COMPILED 0 )

# set target environment
set( CMAKE_FIND_ROOT_PATH ${TOOL_ROOT} )

# search for programs in the build host directories
set( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )

# for libraries and headers in the target directories
set( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )

message( STATUS "Cross-compiling with the TI_cgt_arm_5.2.6 toolchain" )

# include compiler specific headers
find_path( TOOLCHAIN_INCLUDE stdlib.h PATHS "${TOOL_ROOT}/include" )
if( TOOLCHAIN_INCLUDE )
    include_directories( SYSTEM ${TOOLCHAIN_INCLUDE} )
else()
    message( FATAL_ERROR "Failed to find stdlib.h in ${TOOL_ROOT}/include" )
endif()

#################################################################
# Specify statically the build type
#################################################################

# Specifies the build type on single-configuration generators
# and can be overwritten by command line option (CACHE)
# -DCMAKE_BUILD_TYPE=Debug
# -DCMAKE_BUILD_TYPE=Release
# -DCMAKE_BUILD_TYPE=RelWithDebInfo
# -DCMAKE_BUILD_TYPE=MinSizeRel
set( CMAKE_BUILD_TYPE Debug CACHE STRING "Used Build Type" )

#################################################################
# Set Compiler Flags for TI ARM Compiler
#################################################################

set( CMAKE_C_FLAGS                  "" )
set( CMAKE_CXX_FLAGS                "" )
set( CMAKE_ASM_FLAGS                "" )

set( CMAKE_C_FLAGS_DEBUG            ""  CACHE STRING "c flags, build type Debug"            FORCE )
set( CMAKE_C_FLAGS_RELEASE          ""  CACHE STRING "c flags, build type Release"          FORCE )
set( CMAKE_C_FLAGS_RELWITHDEBINFO   ""  CACHE STRING "c flags, build type RelWithDebInfo"   FORCE )

set( CMAKE_CXX_FLAGS_DEBUG          ""  CACHE STRING "c++ flags, build type Debug"          FORCE )
set( CMAKE_CXX_FLAGS_RELEASE        ""  CACHE STRING "c++ flags, build type Release"        FORCE )
set( CMAKE_CXX_FLAGS_RELWITHDEBINFO ""  CACHE STRING "c++ flags, build type RelWithDebInfo" FORCE )

set( CMAKE_ASM_FLAGS_DEBUG          ""  CACHE STRING "ASM flags, build type Debug"          FORCE )
set( CMAKE_ASM_FLAGS_RELEASE        ""  CACHE STRING "ASM flags, build type Release"        FORCE )
set( CMAKE_ASM_FLAGS_RELWITHDEBINFO ""  CACHE STRING "ASM flags, build type RelWithDebInfo" FORCE )

set( CMAKE_EXE_LINKER_FLAGS         ""  CACHE STRING "executable linker flags"              FORCE )

set( COMPILER_FLAGS_NONE
    --symdebug:dwarf                    # Full symbolic debug
)

set( COMPILER_FLAGS_DEBUG
    --silicon_version=7R4               # Target processor version (when not specified, compiler defaults to --silicon_version=4)
    --code_state=32                     # Designate code state, 16-bit (thumb) or 32-bit
    --float_support=VFPv3D16            # Specify floating point support
    --abi=eabi                          # Application binary interface. (when not specified, compiler defaults to --abi=eabi)
    --opt_level=off                     # Optimization level (argument optional, defaults to: 3)
    --fp_mode=strict                    # Floating Point mode (when not specified, compiler defaults to --fp_mode=strict)
    --opt_for_speed=1                   # Speed vs. size trade-offs  (argument optional, defaults to: 4)
    --symdebug:dwarf                    # Full symbolic debug
    --symdebug:dwarf_version=3          # Specify DWARF version
    --diag_warning=225                  # Treat diagnostic <id> as warning, see hint below
    --display_error_number              # Emit diagnostic identifier numbers
    --diag_wrap=off                     # Wrap diagnostic messages (argument optional, defaults to: on)
    --unaligned_access=on               # Generate unaligned loads and stores (when not specified, compiler defaults to --unaligned_access=off)
    --common=on                         # Use ELF common symbols
    --embedded_constants=on             # Specify whether constants can be embedded in code sections (when not specified, compiler defaults to --embedded_constants=on)
    --plain_char=unsigned               # Specify how to treat plain chars (signed/unsigned) (when not specified, compiler defaults to --plain_char=unsigned)
    --enum_type=packed                  # Designate enum type (Default is packed for EABI) (when not specified, compiler defaults to --enum_type=unpacked)
    --wchar_t=16                        # Set the size (in bits) of the C/C++ type wchar_t (16,32) (when not specified, compiler defaults to --wchar_t=16)
)

# hint to diag_warning=225: warning #225-D: function declared implicitly
# This is a dangerous warning, because if the compiler's guess does not match
# the actual function definition, the program will most likely have a difficult
# to diagnose run-time error. 
# see http://processors.wiki.ti.com/index.php/Compiler/diagnostic_messages/225 

set( COMPILER_FLAGS_RELEASE
    --silicon_version=7R4               # Target processor version (when not specified, compiler defaults to --silicon_version=4)
    --code_state=32                     # Designate code state, 16-bit (thumb) or 32-bit
    --float_support=VFPv3D16            # Specify floating point support
    --abi=eabi                          # Application binary interface. (when not specified, compiler defaults to --abi=eabi)
    --opt_level=4                       # Optimization level (argument optional, defaults to: 3)
    --fp_mode=strict                    # Floating Point mode (when not specified, compiler defaults to --fp_mode=strict)
    --opt_for_speed=5                   # Speed vs. size trade-offs  (argument optional, defaults to: 4)
    --symdebug:none                     # Full symbolic debug
    --diag_warning=225                  # Treat diagnostic <id> as warning
    --display_error_number              # Emit diagnostic identifier numbers
    --diag_wrap=off                     # Sets diagnostic messages to wrap at 79 columns (on, which is the default) or not (off)
    --verbose_diagnostics               # Provides verbose diagnostic messages that display the original source with line-wrap and indicate the position of the error in the source line
    --write_diagnostics_file            # Produces a diagnostic message information file with the same source file name with an .err extension
    --unaligned_access=on               # Generate unaligned loads and stores (when not specified, compiler defaults to --unaligned_access=off)
    --common=on                         # Use ELF common symbols
    --embedded_constants=on             # Specify whether constants can be embedded in code sections (when not specified, compiler defaults to --embedded_constants=on)
    --plain_char=unsigned               # Specify how to treat plain chars (signed/unsigned) (when not specified, compiler defaults to --plain_char=unsigned)
    --enum_type=packed                  # Designate enum type (Default is packed for EABI) (when not specified, compiler defaults to --enum_type=unpacked)
    --wchar_t=16                        # Set the size (in bits) of the C/C++ type wchar_t (16,32) (when not specified, compiler defaults to --wchar_t=16)
    #--run_linker                        # --run_linker,-z[=arguments]
)

set( COMPILER_FLAGS_RELDEB
    --silicon_version=7R4               # Target processor version (when not specified, compiler defaults to --silicon_version=4)
    --code_state=32                     # Designate code state, 16-bit (thumb) or 32-bit
    --float_support=VFPv3D16            # Specify floating point support
    --abi=eabi                          # Application binary interface. (when not specified, compiler defaults to --abi=eabi)
    --opt_level=4                       # Optimization level (argument optional, defaults to: 3)
    --fp_mode=strict                    # Floating Point mode (when not specified, compiler defaults to --fp_mode=strict)
    --opt_for_speed=5                   # Speed vs. size trade-offs  (argument optional, defaults to: 4)
    --symdebug:dwarf                    # Full symbolic debug
    --symdebug:dwarf_version=3          # Specify DWARF version
    --diag_warning=225                  # Treat diagnostic <id> as warning
    --display_error_number              # Emit diagnostic identifier numbers
    --diag_wrap=off                     # Sets diagnostic messages to wrap at 79 columns (on, which is the default) or not (off)
    --verbose_diagnostics               # Provides verbose diagnostic messages that display the original source with line-wrap and indicate the position of the error in the source line
    --write_diagnostics_file            # Produces a diagnostic message information file with the same source file name with an .err extension
    --unaligned_access=on               # Generate unaligned loads and stores (when not specified, compiler defaults to --unaligned_access=off)
    --common=on                         # Use ELF common symbols
    --embedded_constants=on             # Specify whether constants can be embedded in code sections (when not specified, compiler defaults to --embedded_constants=on)
    --plain_char=unsigned               # Specify how to treat plain chars (signed/unsigned) (when not specified, compiler defaults to --plain_char=unsigned)
    --enum_type=packed                  # Designate enum type (Default is packed for EABI) (when not specified, compiler defaults to --enum_type=unpacked)
    --wchar_t=16                        # Set the size (in bits) of the C/C++ type wchar_t (16,32) (when not specified, compiler defaults to --wchar_t=16)
)

# When we break up long strings in CMake we get semicolon
# separated lists, undo this here...
string( REGEX REPLACE ";" " " COMPILER_FLAGS_NONE    "${COMPILER_FLAGS_NONE}"    )
string( REGEX REPLACE ";" " " COMPILER_FLAGS_DEBUG   "${COMPILER_FLAGS_DEBUG}"   )
string( REGEX REPLACE ";" " " COMPILER_FLAGS_RELEASE "${COMPILER_FLAGS_RELEASE}" )
string( REGEX REPLACE ";" " " COMPILER_FLAGS_RELDEB  "${COMPILER_FLAGS_RELDEB}"  )

#################################################################
# Set Linker Flags
#################################################################

set( LINKER_FLAGS
    --heap_size=0x800                       # Heap size for C/C++ dynamic memory allocation
    --stack_size=0x4000                     # Set C system stack size
    --search_path=\"${TOOL_ROOT}/lib\"      # Add <dir> to library search path
    --search_path=\"${TOOL_ROOT}/include\"  # Add <dir> to library search path
    --warn_sections                         # Warn if an unspecified output section is created
    --display_error_number                  # Emit diagnostic identifier numbers
    --diag_wrap=off                         # Wrap diagnostic messages (argument optional, defaults to: on)
    --rom_model                             # Link using ROM autoinitialization model
    --be32                                  # Link big-endian code in be-32 format
    --unused_section_elimination=on         # Eliminate sections not needed in the executable (argument optional, defaults to: on)
    --scan_libraries                        # Scans all libraries for duplicate symbol definitions
    --priority                              # Satisfies unresolved references by the first library that contains a definition for Section
    --generate_dead_funcs_list=list_of_dead_functions.xml  # containing a list of functions that are never referenced
)

# When we break up long strings in CMake we get semicolon
# separated lists, undo this here...
string( REGEX REPLACE ";" " " LINKER_FLAGS "${LINKER_FLAGS}" )

#################################################################
# Specify Build Types
#################################################################

# do not write 'normal'-Flags to cache.
# They are only used temporary for compiler tests
set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} ${COMPILER_FLAGS_NONE}" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_C_FLAGS}"     )
set( CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} ${CMAKE_C_FLAGS}"     )

# combine flags to C and C++ target cpu flags, CMAKE_BUILD_TYPE=Debug
set( CMAKE_C_FLAGS_DEBUG   "${CMAKE_C_FLAGS_DEBUG} ${COMPILER_FLAGS_DEBUG}"  CACHE STRING "Debug C-Flags"   FORCE )
set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${CMAKE_C_FLAGS_DEBUG}" CACHE STRING "Debug Cpp-Flags" FORCE )
set( CMAKE_ASM_FLAGS_DEBUG "${CMAKE_ASM_FLAGS_DEBUG} ${CMAKE_C_FLAGS_DEBUG}" CACHE STRING "Debug ASM-Flags" FORCE )

# combine flags to C and C++ target cpu flags, CMAKE_BUILD_TYPE=Release
set( CMAKE_C_FLAGS_RELEASE   "${CMAKE_C_FLAGS_RELEASE} ${COMPILER_FLAGS_RELEASE}"  CACHE STRING "Release C-Flags"   FORCE )
set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${CMAKE_C_FLAGS_RELEASE}" CACHE STRING "Release Cpp-Flags" FORCE )
set( CMAKE_ASM_FLAGS_RELEASE "${CMAKE_ASM_FLAGS_RELEASE} ${CMAKE_C_FLAGS_RELEASE}" CACHE STRING "Release ASM-Flags" FORCE )

# combine flags to C and C++ target cpu flags, CMAKE_BUILD_TYPE=RelWithDebInfo
set( CMAKE_C_FLAGS_RELWITHDEBINFO   "${CMAKE_C_FLAGS_RELWITHDEBINFO}   ${COMPILER_FLAGS_RELDEB}"        CACHE STRING "Release/Debug C-Flags" FORCE )
set( CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} ${CMAKE_C_FLAGS_RELWITHDEBINFO}" CACHE STRING "Release/Debug C-Flags" FORCE )
set( CMAKE_ASM_FLAGS_RELWITHDEBINFO "${CMAKE_ASM_FLAGS_RELWITHDEBINFO} ${CMAKE_C_FLAGS_RELWITHDEBINFO}" CACHE STRING "Release/Debug C-Flags" FORCE )

# set linker flags in necessary sequence
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LINKER_FLAGS}" CACHE STRING "Linker Flags" FORCE )

set( BUILD_SHARED_LIBS OFF )
